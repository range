#!/bin/sh
set -eu

TMPDIR="."
TMPDIR=$TMPDIR/range-tests
mkdir -p "$TMPDIR"

setup() {
    printf "[----------] Test environment set-up.\n"
}

teardown() {
    rm -rf "$TMPDIR"
    printf "[----------] Test environment teardown.\n"
}

trap teardown EXIT

npass=0
nfail=0

run_tests() {
    printf "[==========] Running %d test cases.\n" "$#"

    for t in "$@"
    do
        printf "[ %-8s ] %s\n" "RUN" "$t"

        if "$t"
        then
            printf "[ %8s ] %s\n" "OK" "$t"
            npass=$((npass+1))
        else
            printf "[ %8s ] %s\n" "NOK" "$t"
            nfail=$((nfail+1))
        fi
    done
    
    printf "[==========] %d test cases ran.\n" "$#"
    printf "[  PASSED  ] %d tests.\n" $npass
    printf "[  FAILED  ] %d tests.\n" $nfail

    if [ $nfail -ne 0 ]
    then
        exit 1
    fi
}

range=$(find .. -type f -name "range")

should_handle_increasing_num_range() {
    ct="$(${range} 0 10)"
    ref="$(seq 0 10)"

    [ "$ct" = "$ref" ]
}

should_handle_decreasing_num_range() {
    ct="$(${range} 20 2)"
    ref="$(seq 20 -1 2)"

    [ "$ct" = "$ref" ]
}

should_handle_increasing_char_range() {
    ct="$(${range} 'a' 'd')"
    ref="$(printf 'a\nb\nc\nd')"

    [ "$ct" = "$ref" ]
}

should_handle_decreasing_char_range() {
    ct="$(${range} 'e' 'b')"
    ref="$(printf 'e\nd\nc\nb')"

    [ "$ct" = "$ref" ]

}

should_handle_explicit_increment() {
    ct="$(${range} 0 2 100)"
    ref="$(seq 0 2 100)"

    [ "$ct" = "$ref" ]
}

should_handle_explicit_decrement() {
    ct="$(${range} 100 -2 0)"
    ref="$(seq 100 -2 0)"

    [ "$ct" = "$ref" ]
}

should_handle_sflag() {
    ct="$(${range} -s , 1 3 30)"

    ref=""
    i=1
    while [ $i -le 30 ]; do
        ref="$(printf "$ref%d," $i)"
        i=$(( i+3 ))
    done

    [ "$ct" = "$ref" ]
}

should_handle_tflag() {
    ct="$(${range} -t '\n' 1 3 30)"
    ref="$(seq 1 3 30)"

    # GNU seq doesn't support the -t flag
    ref=$(printf "%s\n" "$ref")

    [ "$ct" = "$ref" ]
}

should_handle_singleton() {
    ct="$(${range} 44 44)"

    [ "$ct" = "44" ]
}

setup
run_tests \
    should_handle_increasing_num_range \
    should_handle_decreasing_num_range \
    should_handle_increasing_char_range \
    should_handle_decreasing_char_range \
    should_handle_explicit_increment \
    should_handle_explicit_decrement \
    should_handle_sflag \
    should_handle_tflag \
    should_handle_singleton
