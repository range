# range
The **range** utility prints a sequence of numbers or characters in the range [first, last] in the given number of steps.
Both the separator between each element of the sequence and the terminator can be specified.

## Features
* Quality
    * Compiled with security hardening flags.
    * Static analysis integrated using clang's `scan-build` using checkers `alpha.security`, `alpha.core.CastSize`,
    `alpha.core.CastToStruct`, `alpha.core.IdenticalExpr`, `alpha.core.PointerArithm`, `alpha.core.PointerSub`,
    `alpha.core.SizeofPtr`, `alpha.core.TestAfterDivZero`, `alpha.unix`.
    * Test harness
    * Follows [FreeBSD coding style](https://www.freebsd.org/cgi/man.cgi?query=style&sektion=9).
* Portable
    * C99 compliant *and* may be built in an environment which provides POSIX.1-2001 system interfaces.
    * Self-contained, no external dependencies
    * Easy to compile and uses POSIX make.

## Limitations
* The minimum and maximum values for the `first`, `last` and `increment` parameters are `INT_MIN` and `INT_MAX`
respectively.

## Build dependencies
The only dependency is a toolchain supporting the following flags:

```
CFLAGS = -std=c99 -O2 -Wall -Wextra -Wpedantic \
	-Walloca -Wcast-qual -Wconversion -Wformat=2 -Wformat-security \
	-Wnull-dereference -Wstack-protector -Wvla -Warray-bounds \
	-Wbad-function-cast -Wconversion -Wshadow -Wstrict-overflow=4 -Wundef \
	-Wstrict-prototypes -Wswitch-default -Wfloat-equal -Wimplicit-fallthrough \
	-Wpointer-arith -Wswitch-enum \
	-D_FORTIFY_SOURCE=2 \
	-fstack-protector-strong -fPIE -fstack-clash-protection

LDFLAGS = -Wl,-z,relro -Wl,-z,now -Wl,-z,noexecstack -Wl,-z,separate-code
```

Otherwise you can just remove the security flags and compile it with
```
CFLAGS = -std=c99 -O2 -Wall -Wextra -Wpedantic
LDFLAGS =
```

or pass your own flags to make
```sh
make CC=gcc CFLAGS=... LDFLAGS=...
```

## Installation
Clone this repository then

```sh
$ make PREFIX=/usr install
```

This will install the compiled binary under `PREFIX` (`/usr/bin`) in this case, if not specified `PREFIX` will default
to `/usr/local`. For staged installs, `DESTDIR` is also supported. As the binary does not have any dependency it does
not have to be installed before use.

## Usage
**range** receives as input the bounds of the range and an optional increment (positive or negative). In case the
increment is omitted it defaults either 1 or -1 depending on whether the lower bound is greater than the upper bound
or viceversa.

The options are as follows:

* **-h** Print usage information and exit.
* **-s** Specify a string to be used to delimit each element of the sequence. If omitted `'\n'` is used.
* **-t** Specify a string to be used to end the sequence.

### Examples
```sh
$ range 0 2 20
0
2
4
6
8
10
12
14
16
18
20
```

```sh
$ range -s , 'a' 'z'
a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,
```

```sh
$ range 10 5
10
9
8
7
6
5
```

```sh
$ range -s "\t" -t "\n" 1 10
1	2	3	4	5	6	7	8	9	10	
```

### Test suite
The test suite consists of a POSIX shell script called `harness.sh` contained in the `test` folder. It's output is
similar to [googletest](https://github.com/google/googletest)'s and it can be invoked with `make test` which, if
everything is working should output something similar to
```sh
(cd test && ./harness.sh)
[----------] Test environment set-up.
[==========] Running 9 test cases.
[ RUN      ] should_handle_increasing_num_range
[       OK ] should_handle_increasing_num_range
[ RUN      ] should_handle_decreasing_num_range
[       OK ] should_handle_decreasing_num_range
[ RUN      ] should_handle_increasing_char_range
[       OK ] should_handle_increasing_char_range
[ RUN      ] should_handle_decreasing_char_range
[       OK ] should_handle_decreasing_char_range
[ RUN      ] should_handle_explicit_increment
[       OK ] should_handle_explicit_increment
[ RUN      ] should_handle_explicit_decrement
[       OK ] should_handle_explicit_decrement
[ RUN      ] should_handle_sflag
[       OK ] should_handle_sflag
[ RUN      ] should_handle_tflag
[       OK ] should_handle_tflag
[ RUN      ] should_handle_singleton
[       OK ] should_handle_singleton
[==========] 9 test cases ran.
[  PASSED  ] 9 tests.
[  FAILED  ] 0 tests.
[----------] Test environment teardown.
```

### Static analysis
Static analysis on the code base is done by using clang's static analyzer run through `scan-build.sh` which wraps the
`scan-build` utility. The checkers used are part of the
[Experimental Checkers](https://releases.llvm.org/12.0.0/tools/clang/docs/analyzer/checkers.html#alpha-checkers)
(aka *alpha* checkers):

* `alpha.security`
* `alpha.core.CastSize`
* `alpha.core.CastToStruct`
* `alpha.core.IdenticalExpr`
* `alpha.core.PointerArithm`
* `alpha.core.PointerSub`
* `alpha.core.SizeofPtr`
* `alpha.core.TestAfterDivZero`
* `alpha.unix`

## Contributing
Send patches on the [mailing list](https://www.freelists.org/list/range-dev), report bugs using [git-bug](https://github.com/MichaelMure/git-bug/). 

## License
BSD 2-Clause FreeBSD License, see LICENSE.

