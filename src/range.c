/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2022 Alessio Chiapperini
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _POSIX_C_SOURCE
#  define _POSIX_C_SOURCE 200112L
#elif _POSIX_C_SOURCE < 200112L
#  error incompatible _POSIX_C_SOURCE level
#endif

#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static char *sep = "\n";	/* separator */
static char *term = 0;		/* terminator */
static int isnum = 0;		/* whether or not both first and last are
				   numbers */

static void
usage(void)
{
	(void)fprintf(stderr, ""
	    "usage: range [-h] [-s sep] [-t term] first last\n"
	    "       range [-h] [-s sep] [-t term] first increment last\n");
	exit(1);
}

/*
 * Replaces escaped strings with its control character.
 */
static char *
unescape(char *str)
{
	char *cstr, *nstr = str;

	for (cstr = str; (*str = *cstr) != 0; cstr++, str++) {
		if (*cstr == '\\') {
			switch (*++cstr) {
			case 'a':	/* alert (bell) */
				*str = '\a';
				break;
			case 'b':	/* backspace */
				*str = '\b';
				break;
			case 'f':	/* formfeed */
				*str = '\f';
				break;
			case 'n':	/* newline */
				*str = '\n';
				break;
			case 'r':	/* carriage return */
				*str = '\r';
				break;
			case 't':	/* horizontal tab */
				*str = '\t';
				break;
			case 'v':	/* vertical tab */
				*str = '\v';
				break;
			case '\\':	/* backslash */
				*str = '\\';
				break;
			case '\'':	/* single quote */
				*str = '\'';
				break;
			case '\"':	/* double quote */
				*str = '\"';
				break;
			default:
				--cstr;
			}
		}
	}
	return (nstr);
}

/*
 * Verifiy that the string is numeric and return the corresponding number if so.
 */
static int
number(const char *str, int *num)
{
	long sl;
	int ret;
	char *end;

	ret = 1;
	sl = strtol(str, &end, 10);
	if (end == str) {
		ret = 0;
	} else if (*end != '\0') {
		ret = 0;
	} else if ((sl == LONG_MIN || sl == LONG_MAX) && errno == ERANGE) {
		perror("range");
		exit(1);
	} else if (sl > INT_MAX || sl < INT_MIN) {
		errno = ERANGE;
		perror("range");
		exit(1);
	} else if (num != 0) {
		*num = (int)sl;
	}

	return (ret);
}

/*
 * Print numbers or characters in the interval [first, last] by the given
 * increment.
 */
static void
print_range(int first, int increment, int last)
{
	for (; increment > 0 ? first <= last : first >= last;
	    first+=increment) {
		if (isnum == 0) {
			(void)printf("%c", first);
		} else {
			(void)printf("%d", first);
		}
		fputs(sep, stdout);
	}
	if (term != 0) {
		fputs(term, stdout);
	}
}

int
main(int argc, char *argv[])
{
	int opt;
	int first, increment, last;

	while ((optind < argc) && !number(argv[optind], 0) &&
	    (opt = getopt(argc, argv, "hs:t:")) != -1) {
		switch (opt) {
		case 'h':
			usage();
			break;
		case 's':
			sep = unescape(optarg);
			break;
		case 't':
			term = unescape(optarg);
			break;
		case '?':
			/* FALLTHROUGH */
		default:
			usage();
		}
	}

	argc -= optind;
	argv += optind;

	if (argc < 2 || argc > 3) {
		usage();
		return (1);
	}

	first = 0;
	increment = 0;
	last = 0;
	if ((isnum = number(argv[argc - 1], &last)) == 0) {
		if (strlen(argv[argc - 1]) > 1) {
			(void)fprintf(stderr, "range: last must be a single "
			    "character or a number\n");
			return (1);
		}
		last = argv[argc - 1][0];
	}

	if (argc > 1) {
		if ((isnum += number(argv[0], &first)) <= 1) {
			if (strlen(argv[0]) > 1) {
				(void)fprintf(stderr, "range: first must be a "
				    "single character or a number\n");
				return (1);
			}
			first = argv[0][0];
		}
	}

	if (argc > 2) {
		if (number(argv[1], &increment) == 0) {
			(void)fprintf(stderr, "range: %screment must be a "
			    "number\n", (first < last) ? "in" : "de");
			return (1);
		}

		if (increment == 0) {
			(void)fprintf(stderr, "range: %screment cannot be 0\n",
			    (first < last) ? "in" : "de");
			return (1);
		}
	}

	if (isnum == 1) {
		(void)fprintf(stderr, "range: first and last must both be "
		    "either a number or a character\n");
		return (1);
	}

	if (increment == 0) {
		increment = (first < last) ? 1 : -1;
	}

	if (increment < 0 && first < last) {
		(void)fprintf(stderr, "range: increment must be positive\n");
		return (1);
	}

	if (increment > 0 && first > last) {
		(void)fprintf(stderr, "range: increment must be negative\n");
		return (1);
	}

	print_range(first, increment, last);

	return (0);
}

